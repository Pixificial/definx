/*************************************************************************
    This file is part of definx.
    Copyright (C) 2021 Abdullah Çırağ

    definx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    definx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/
#ifndef _DICTIONARIES_H_
#define _DICTIONARIES_H_

struct dictionary {
	char dictionary_name[32];
	char dictionary_url_prefix[128];
	char dictionary_url_suffix[16];
};

extern int dictionary_count;
extern struct dictionary dictionaries[];
extern char *get_dictionary_url(char dictionary_name[], char word[]);

#endif
