/*************************************************************************
    This file is part of definx.
    Copyright (C) 2021 Abdullah Çırağ

    definx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    definx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/
#ifndef _OPTIONS_H_
#define _OPTIONS_H_
#include <unistd.h>
#include <getopt.h>
#include <stdbool.h>

static struct option const longopts[] =
{
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'v'}
};

static char option = '\0';

void print_options_message(char *argv[], char option);

#endif
