# definx - a fast command-line metadictionary.
## About
definx is a quick metadictionary running in the command-line. It is made to be  
used for gathering definitions about a word from multiple dictionaries as the  
source, making it easier to gather more unbiased, closer to objective  
information.
  
## Roadmap
1. Initialisation, modular base code, licence. (Done)
2. Online dictionary parser.
	1. Modular parser base code.
	2. Parsers for online dictionaries.
	3. Custom online dictionary support?
3. Multiple languages support.
	1. A bit of parser rework.
	2. Support for all languages available on the online dictionaries.
  
## Installation
### Requirements:  
1. C compiler of choice (assumed GCC).
2. curl.
  
#### GNU/Linux
1. Clone the repository `git clone https://codeberg.org/Pixificial/definx.git`
2. `cd` into the local directory.
3. Run `gcc main.c options.c dictionaries.c downloader.c -lcurl -o definx`
  
## Usage
**Note:** Do not forget to move the binary to a directory stated in `$PATH`.  
	`definx [OPTIONS] [WORD]`  
  
### After Roadmap 3.2. is done:
`definx [OPTIONS] [WORD]`  
`definx [OPTIONS] [WORD] [LANGUAGE]`  
  
After the definitions are downloaded, you may find them in `/tmp/definx/`.
  
## Contributing, bug reports, ideas, patches, questions
All are encouraged! You are invited to contribute, report bugs, ask  
questions (if any), provide patches and ideas.
  
## License
Copyright (C) 2021 Abdullah Çırağ  
definx is distributed under the terms of the GNU General Public License as  
published by the Free Software Foundation; either version 3 of the License, or  
(at your option) any later version. A copy of this license can be found in the  
file COPYING included with the source code of this program.
