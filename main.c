/*************************************************************************
    definx - a fast command-line metadictionary.
    Copyright (C) 2021 Abdullah Çırağ

    definx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    definx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/
#include <stdio.h>
#include "options.h"
#include "downloader.h"

int non_opt_arg_count;

int
main (int argc, char *argv[])
{
	int c;
	while ((c = getopt_long (argc, argv, "hv", longopts, NULL)) != -1) {
		switch (c) {
			case 'h':
				option = c;
				break;
			case 'v':
				if (option != 'h') {
					option = c;
				}
				break;
		}
	}

	if ((option == 'h') || (option == 'v')) {
		print_options_message(argv, option);
	} else {
		char *non_opt_args[argc];
		for (int i = optind; i < argc; i++) {
			non_opt_args[non_opt_arg_count] = argv[i];
			non_opt_arg_count++;
		}

		switch (non_opt_arg_count) {
			case 0:
				printf ("No arguments were specified, terminating.\nUse %s "
					"--help for more information.\n", argv[0]);
				break;
			case 1:
				printf("You are looking for the definition of the word %s.\n",
					non_opt_args[0]);
				download_definition(argv[0], "wiktionary",
					non_opt_args[0]);
				download_definition(argv[0], "cambridge",
					non_opt_args[0]);
				download_definition(argv[0], "dictionary.com",
					non_opt_args[0]);
				download_definition(argv[0], "merriam-webster",
					non_opt_args[0]);
				download_definition(argv[0], "oxford",
					non_opt_args[0]);
				download_definition(argv[0], "thefreedictionary",
					non_opt_args[0]);
				break;
			default:
				printf ("Unrecognised usage. Use %s --help for more "
					"information.\n", argv[0]);
				break;
		}
	}
	return 0;
}
