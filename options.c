/*************************************************************************
    This file is part of definx.
    Copyright (C) 2021 Abdullah Çırağ

    definx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    definx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/
#include "options.h"
#include <stdio.h>
#include "version.h" /* For project_name and version */

void print_options_message(char *argv[], char option)
{
	switch (option) {
		case 'h':
			printf("Usage: %s [OPTIONS] [WORD]\n", argv[0]);
			printf("Download definitions of words from various online "
				"dictionaries.\n\n");
			printf("Web pages containing the definitions can be located in:\n"
				"/tmp/definx/<dictionary_name>\n\n");
			printf("Options:\n");
			printf("    -h, --help        Display this help and exit\n");
			printf("    -v, --version     Display version and licence "
				"information\n");
			break;
		case 'v':
			printf("%s %s\n", project_name, version);
			printf("Copyright (C) 2021 Abdullah Çırağ\n");
			printf("%s is free software licenced under GPLv3+.\n",
				project_name);
			printf("You are free to use, study, change or redistribute it.\n");
			printf("There is NO WARRANTY, to the extent permitted by law.\n");
	}
}
