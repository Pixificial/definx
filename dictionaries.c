/*************************************************************************
    This file is part of definx.
    Copyright (C) 2021 Abdullah Çırağ

    definx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    definx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/
#include "dictionaries.h"
#include <string.h>
#include <stdio.h>

int dictionary_count = 6;
struct dictionary dictionaries[] = {
	{"wiktionary", "https://en.wiktionary.org/wiki/", "\0"},
	{"cambridge", "https://dictionary.cambridge.org/dictionary/english/", "\0"},
	{"dictionary.com", "https://www.dictionary.com/browse/", "\0"},
	{"merriam-webster", "https://www.merriam-webster.com/dictionary/", "\0"},
	{"oxford", "https://www.lexico.com/definition/", "\0"},
	{"thefreedictionary", "https://www.thefreedictionary.com/", "\0"}
};

char dictionary_url[255];

char *get_dictionary_url(char dictionary_name[], char word[])
{
	memset(&dictionary_url, '\0', sizeof(dictionary_url));
	
	for (int i = 0; i < dictionary_count; i++) {
		if (!strcmp(dictionary_name, dictionaries[i].dictionary_name)) {
			strcat(dictionary_url, dictionaries[i].dictionary_url_prefix);
			strcat(dictionary_url, word);
			strcat(dictionary_url, dictionaries[i].dictionary_url_suffix);
			break;
		}
	}
	return dictionary_url;
}
