/*************************************************************************
    This file is part of definx.
    Copyright (C) 2021 Abdullah Çırağ

    definx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    definx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/
#include "downloader.h"
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <curl/curl.h>
#include "dictionaries.h"

FILE *
download_definition(char project_name[], char dictionary_name[], 
		char word[])
{
	CURL *curl;
	curl = curl_easy_init();
	if (curl) {
		char dictionary_data_directory[5 + strlen(project_name) +
			strlen(dictionary_name)]; /* 4 for 6 characters - 2 escape seq. */
		memset(&dictionary_data_directory, '\0',
			sizeof(dictionary_data_directory));

		strcat(dictionary_data_directory, "/tmp/");
		strcat(dictionary_data_directory, project_name);

		int err = mkdir(dictionary_data_directory,
			S_IRUSR | S_IWUSR | S_IXUSR);
		if (err <= 0) {
			strcat(dictionary_data_directory, "/");
			strcat(dictionary_data_directory, dictionary_name);

			FILE *dictionary_data_file = fopen(dictionary_data_directory,
					"w+");

			if(dictionary_data_file) {
				CURLcode res;
				curl_easy_setopt(curl, CURLOPT_URL,
					get_dictionary_url(dictionary_name, word));
				curl_easy_setopt(curl, CURLOPT_WRITEDATA,
					dictionary_data_file);
				res = curl_easy_perform(curl);
  				curl_easy_cleanup(curl);

				return dictionary_data_file;
			} else {
				printf("File creation failed.\n");
				return NULL;
			}
		} else {
			printf("Directory creation failed.\n");
			return NULL;
		}
	} else {
		printf("Something went wrong with cURL.\n");
		return NULL;
	}
}
